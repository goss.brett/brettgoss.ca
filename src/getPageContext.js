import { SheetsRegistry } from 'jss';
import { createMuiTheme, createGenerateClassName } from '@material-ui/core/styles';

import blue from '@material-ui/core/colors/blue';
import green from '@material-ui/core/colors/green';

// A theme with custom primary and secondary color.
// It's optional.
const theme = createMuiTheme({
  palette: {
    primary: {
      light: blue[300],
      main: blue[500],
      dark: blue[700],
    },
    secondary: {
      light: green[300],
      main: green[500],
      dark: green[700],
    },
  },
  overrides: {
    MuiBottomNavigation: { // Name of the component ⚛️ / style sheet
      root: { // Name of the rule
        marginTop: '51vh',
        borderTop: '1px solid rgba(120,120,120,.08)',
        color: blue[700],
        padding: '5rem 0',
        backgroundColor: 'rgba(120,120,120,.05)',
        boxShadow: 'inset 0 0.5rem 0.5rem -0.5rem rgba(0,0,0,.05)',
      },
    },
    MuiTypography: {
      root: {
        fontFamily: '"Open Sans", Arial, sans-serif',
      },
      h4: {
        fontWeight: '600',
        color: '#555',
      },
      body2: {
        whiteSpace: 'pre-line',
      }
    }
  },
  typography: {
    useNextVariants: true,
  },
});

function createPageContext() {
  return {
    theme,
    // This is needed in order to deduplicate the injection of CSS in the page.
    sheetsManager: new Map(),
    // This is needed in order to inject the critical CSS.
    sheetsRegistry: new SheetsRegistry(),
    // The standard class name generator.
    generateClassName: createGenerateClassName(),
  };
}

let pageContext;

export default function getPageContext() {
  // Make sure to create a new context for every server-side request so that data
  // isn't shared between connections (which would be bad).
  if (!process.browser) {
    return createPageContext();
  }

  // Reuse context on the client-side.
  if (!pageContext) {
    pageContext = createPageContext();
  }

  return pageContext;
}