import Layout from '../components/layout';
import Me from '../components/Me'

import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/core/Link";

function Home() {
  return (
    <Layout>
      <Typography variant="h4">
        Hey there, I'm Brett!
      </Typography>

      <Typography style={{ maxWidth: '21rem' }} component="p">
      {`
        I'm a software developer with a background of customer service and hospitality.

        I love the outdoors, and am often out hiking, camping or gardening.

        My website is still a work in progress, so stay tuned for more!

        In the meantime, the best way to contact me is by email at `}

        <Link target='_blank' href='mailto:goss.brett@gmail.com'>
          goss.brett@gmail.com
        </Link>{`.

      `}
      </Typography>

      <Me />

    </Layout>
  )
}

export default Home