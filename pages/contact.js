import Layout from '../components/layout';

function Contact() {
  return (
    <Layout title='Contact'>
      Contact me by email at goss.brett@gmail.com
    </Layout>
  )
}

export default Contact