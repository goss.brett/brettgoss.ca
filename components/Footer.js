import React from "react";
import PropTypes from "prop-types";

import { withStyles } from "@material-ui/core/styles";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

import SocialIcons from "./icons/SocialIcons";

const styles = {
  root: {
    flexGrow: 1,
  }
};

function Footer(props) {
  const { classes } = props;

  return (
    <footer className={classes.root}>
      <BottomNavigation position="static" color="secondary">
        <Toolbar>
          {/* <Typography variant="h6">This is the footer</Typography> */}
          <SocialIcons />
        </Toolbar>
      </BottomNavigation>
    </footer>
  );
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Footer);
