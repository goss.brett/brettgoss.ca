import React from "react";
import PropTypes from "prop-types";

import { withStyles } from "@material-ui/core/styles";

const styles = {
  root: {
    display: 'flex',
    borderRadius: '50%',
    backgroundColor: '#d6e1ed',
    textAlign: 'center',
    height: '10rem',
    width: '10rem',
    margin: 'auto',
  }
};

function Me(props) {
  const { classes } = props;

  return (
    <img className={classes.root} src='../static/images/me.jpg' />
  );
}

Me.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Me);
