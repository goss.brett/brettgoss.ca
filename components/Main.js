import React from "react";
import PropTypes from "prop-types";

import { withStyles } from "@material-ui/core/styles";

const styles = {
  root: {
    flexGrow: 1,
    flexWrap: 'wrap',
    display: 'flex',
    padding: '5rem 1.5rem',
    maxWidth: '40rem',
    margin: 'auto',
  }
};

function Main(props) {
  const { classes, children } = props;

  return (
    <main className={classes.root}>
        {children}
    </main>
  );
}

Main.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Main);
