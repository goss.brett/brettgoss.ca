import Head from 'next/head'

import NavBar from './NavBar';
import Main from './Main';
import Footer from './Footer';

export default ({ children, title = 'Home' }) => (
  <div>
    <Head>
      <title>{title} | brettgoss.ca</title>
      <meta charSet='utf-8' />
      <meta
        name="viewport"
        content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no"
      />
      {/* TODO: Don't use a CDN for these */}
      <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css' />
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" />
      <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
      <link rel="shortcut icon" type="image/x-icon" href="/static/favicon.ico" />
    </Head>

    <header>
      <NavBar />
    </header>

    <Main>
      {children}
    </Main>

    <Footer />
  </div>
)