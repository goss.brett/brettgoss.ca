import React from 'react';
import PropTypes from 'prop-types';
// import Link from 'next/link'

import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Link from '@material-ui/core/Link';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const styles = {
  root: {
    flexGrow: 1,
    paddingTop: '3rem',
  },
  grow: {
    flexGrow: 1,
  }
};

function NavBar(props) {
  const { classes } = props;

  return (
    <nav className={classes.root}>
      <AppBar position="fixed" color="default">
        <Toolbar>
          <Typography variant="h6" color="inherit" className={classes.grow}>
            Brett Goss
          </Typography>

          <Link href='/'>
            <Button>
              Home
            </Button>
          </Link>

          {/* {'|'}

          <Link href='/about'>
            <Button>
              About
            </Button>
          </Link> */}

          {/* {'|'}

          <Link href='/contact'>
            <Button>
              Contact
            </Button>
          </Link> */}
        </Toolbar>
      </AppBar>
    </nav>
  );
}

NavBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NavBar);